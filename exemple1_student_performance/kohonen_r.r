#chargement des données

Observations_math=read.table("C:\\Users\\meryem\\Desktop\\student-mat1.csv",sep=";",header=TRUE)
Observations_port=read.table("C:\\Users\\meryem\\Desktop\\student-por1.csv",sep=";",header=TRUE)

#affichage des statistiques descriptives

print(summary(Observations_math))
print(summary(Observations_port))

#fusionnement

flux_données=merge(Observations_math,Observations_port,by=c("school","sex","age","address","famsize","Pstatus","Medu","Fedu","Mjob","Fjob","reason","nursery","internet"))

print(nrow(flux_données)) # 382 studen

plot(flux_données)

#centrer et reduire les données

Z <- scale(flux_données,center=T,scale=T)

#utilisation de la bibliothèque kohonen

library(kohonen)

# génération de la carte

carte <- som(Z,grid=somgrid(15,10,"hexagonal"))

print(carte$grid)

#définition des couleurs

degrade.bleu <- function(n){
  return(rgb(0,0.4,1,alpha=seq(0,1,1/n)))
}


plot(carte,type="count",palette.name=degrade.bleu)

print(carte$unit.classif)

nb <- table(carte$unit.classif)

print(nb)

print(length(nb))

plot(carte,type="dist.neighbours")

plot(carte,type="codes",codeRendering = "segments")

print(carte$codes[1:2,])

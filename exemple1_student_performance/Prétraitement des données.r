#1)Cleaning
#importation des donnees math et portugais

d1=read.table("C:/Users/ibtissam/Desktop/student/student-mat.csv"",sep=";",header=TRUE)
d2=read.table("C:/Users/ibtissam/Desktop/student/student-por.csv",sep=";",header=TRUE)

#simplifier la manipulation de tables de données
library(dplyr)

#la jointure des deux tables de donnees
d3=merge(d1,d2,by=c("school","sex","age","address","famsize","Pstatus","Medu","Fedu","Mjob","Fjob","reason","nursery","internet"))

#afficher le nombre d'etudiants
print(nrow(d3)) # 382 etudiants

#Donner un id a tous les etudiants
#mettre l'id au debut 
d3$id = 1:nrow(d3) 


# permet de sélectionner l'id et la note finale "portugais"
d3 = select(d3,id,school:G3.y)

#2-Reduction
#on supprime les variables reason et school
d3$reason<-NULL
d3$school<-NULL

#3-Transformation
#remplacer yes par 1 et no par 0
#yes=>1 , No=>0

d3$schoolsup.x=factor(d3$schoolsup.x,levels=c("yes","no"),labels=c(1,0))
d3$schoolsup.y=factor(d3$schoolsup.y,levels=c("yes","no"),labels=c(1,0))

d3$famsup.x=factor(d3$famsup.x,levels=c("yes","no"),labels=c(1,0))
d3$famsup.y=factor(d3$famsup.y,levels=c("yes","no"),labels=c(1,0))

d3$paid.x=factor(d3$paid.x,levels=c("yes","no"),labels=c(1,0))
d3$paid.y=factor(d3$paid.y,levels=c("yes","no"),labels=c(1,0))

d3$activities.x=factor(d3$activities.x,levels=c("yes","no"),labels=c(1,0))
d3$activities.y=factor(d3$activities.y,levels=c("yes","no"),labels=c(1,0))

d3$nursery=factor(d3$nursery,levels=c("yes","no"),labels=c(1,0))

d3$higher.x=factor(d3$higher.x,levels=c("yes","no"),labels=c(1,0))
d3$higher.y=factor(d3$higher.y,levels=c("yes","no"),labels=c(1,0))

d3$internet=factor(d3$internet,levels=c("yes","no"),labels=c(1,0))

d3$romantic.x=factor(d3$romantic.x,levels=c("yes","no"),labels=c(1,0))
d3$romantic.y=factor(d3$romantic.y,levels=c("yes","no"),labels=c(1,0))
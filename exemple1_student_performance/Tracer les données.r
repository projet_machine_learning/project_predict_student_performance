## Jeu de données sur les performances des étudiants 
student.por <- read.table("C:/Users/ibtissam/Desktop/student/student-por.csv",sep=";")
student.mat <- read.table("C:/Users/ibtissam/Desktop/student/student-mat.csv",sep=";")
attach(student.mat)

#Regardez les noms de variables
names(student.mat)
 
#Regardez un résumé de nos données
summary(student.mat)

#Regardez la structure de nos données
str(student.mat)

#Parcourez nos données
plot(Mjob,G3)
plot(health,G3)
plot(Medu,G3,xlab="Mother's Education",ylab="Final Grade")
plot(Fedu,G3,xlab="Father's Education",ylab="Final Grade")
plot(sex,G3)


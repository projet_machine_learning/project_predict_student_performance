#Taille du jeu de données apres le cleaning = 382 lignes
#Ensemble de formation de 80% = 305 lignes
#Ensemble de test de 20% = 77 lignes

#regression

#305 premieres lignes pour le training
data_train <- d3[1:305, ]

#77 dernieres lignes pour le test
data_test <- d3[306:382, ]


library(randomForest)

#Modélisation sans notes pour le taining
#x=>pour les mathematiques
fit<-randomForest(G3.x ~ sex+age+famsize+traveltime.x+studytime.x+failures.x+schoolsup.x+famsup.x+paid.x+activities.x+nursery+higher.x+internet+romantic.x+famrel.x+freetime.x+goout.x+Dalc.x+Walc.x+health.x+absences.x,data=data_train,importance=TRUE,ntree=1000)
fit
varImpPlot(fit)
 
#y=>pour le portugais
fit1<-randomForest(G3.y ~ sex+age+famsize+traveltime.y+studytime.y+failures.y+schoolsup.y+famsup.y+paid.y+activities.y+nursery+higher.y+internet+romantic.y+famrel.y+freetime.y+goout.y+Dalc.y+Walc.y+health.y+absences.y,data=data_train,importance=TRUE,ntree=1000)
fit1
varImpPlot(fit1)

#Modélisation avec les notes G1 et G2 pour le training
fit2<-randomForest(G3.x ~ sex+age+famsize+traveltime.x+studytime.x+failures.x+schoolsup.x+famsup.x+paid.x+activities.x+nursery+higher.x+internet+romantic.x+famrel.x+freetime.x+goout.x+Dalc.x+Walc.x+health.x+absences.x+G1.x+G2.x,data=data_train,importance=TRUE,ntree=1000)
fit2
varImpPlot(fit2)

fit3<-randomForest(G3.y ~ sex+age+famsize+traveltime.y+studytime.y+failures.y+schoolsup.y+famsup.y+paid.y+activities.y+nursery+higher.y+internet+romantic.y+famrel.y+freetime.y+goout.y+Dalc.y+Walc.y+health.y+absences.y+G1.y+G2.y,data=data_train,importance=TRUE,ntree=1000)
fit3
varImpPlot(fit3)

